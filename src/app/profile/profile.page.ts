import {
  Component,
  OnInit,
  Injectable,
  Pipe
} from '@angular/core';
import { UserService } from '../services/user.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  userinfo = {
    "name": "Customer Name",
    "email": "Customer Email",
    "mobile": "Customer Mobile",
    "eid": "dummy",
    "sub": "dummy",
    "image": "https://secure.gravatar.com/avatar/dummy.jpg?s=256&d=mm&r=x",
    "can_makeSale": false
  }
  sessions = [];
  loggedin = false;
  vfail = false;

  constructor(
    private uS: UserService,
    private inaB: InAppBrowser
  ) {
    var thisER = this;
    try{
      setTimeout(function(){
        thisER.uS.getDetails().then((obj) => {
          obj.subscribe((data: any) => {
            if(data.result == true){
              thisER.userinfo = data.payload;
              thisER.loggedin = true;
            }else{
              thisER.loggedin = false;
              if(!data.login)
                window.location.href = "./auth/login";
            }
          })
        });
        thisER.uS.getSessions().then((obj) => {
          obj.subscribe((data: any) => {
            if(data.result == true)
              thisER.sessions = data.payload.sessions;
          })
        });
      }, 2000);
    }catch(err){
      this.vfail = true;
    }
  }

  ngOnInit() {
    
  }

  tOtA(value) {
    let keys = [];
    for (let key in value) {
        keys.push({key: key, value: value[key]});
    }
    return keys;
  }
  prune(value: string, length: number = 25)
  {
    if(value.length > length)
      return value.substr(0, length-3 ) + '...';
    else
      return value;
  }
  pacReq(){
    var tI = this.inaB.create("https://indiancases.com/requestPackage/" + this.userinfo.eid, "_system", "location=no");
    tI.close();
  }
}
