import {
  Component,
  OnInit
} from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import {
  NavController,
  LoadingController,
  ToastController
} from '@ionic/angular';
import { JudgmentService } from '../services/judgment.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
var PubNub = require('pubnub');

declare var window: any;

@Component({
  selector: 'app-judgment',
  templateUrl: './judgment.page.html',
  styleUrls: ['./judgment.page.scss']
})
export class JudgmentPage implements OnInit {

  payload = {
    "iconumber" : "",
    "court" : "",
    'judges' : "",
    'petitioners' : "",
    'respondents'  : "",
    "equivalents" : [],
    "referredout" : [],
    "mentionedin" : [],
    "casenumbers" : [],
    "headnotes" : [],
    "overruleding" : [],
    "overruled" : [],
    "attachments" : [],
    "properties" : {
      "judgmentcontent" : false,
      "trueprint" : false,
      "lawofland" : true
    },
  };
  jstatus = "lawofland";
  status = 'Judgment loading !';
  id = null;
  loaded = false;
  loader = null;
  phrases = [];
  sub = "";

  constructor(
    private route: ActivatedRoute,
    private storage: Storage,
    protected js: JudgmentService,
    protected navCtrl: NavController,
    public loadingController: LoadingController,
    public inaB: InAppBrowser,
    private clipboard: Clipboard,
    private tC: ToastController) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadInfo();
    var thisER = this;
    this.route.queryParams
      .subscribe(params => {
        if(params['csphrases'] != null)
          thisER.phrases = atob(params['csphrases']).split(',');
        else
          thisER.phrases = [];
      });
    console.log(this.phrases)
    const pubnub2 = new PubNub({
      subscribeKey: 'sub-c-05cb95a8-a69c-11e8-8c35-a6946b4e582d',
      ssl: true
    });
    var thisER = this;
    pubnub2.addListener({
      message: function(m) {
        if(thisER.id != m.message)
          thisER.navCtrl.goRoot('/judgment/' + m.message);
      }
    });
    this.storage.get('ic_sub').then((val) => {
      pubnub2.subscribe({
        channels: [
          val
        ],
      });
    })
  }

  async loadInfo(){
    this.loader = await this.loadingController.create({
      content: 'Loading case info...',
      duration: 200000
    });
    this.loader.present();

    this.js.getCase(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.loaded = true;
        this.payload = data.payload;
        this.status = data.payload.iconumber
        if(!this.payload.properties.lawofland)
          this.jstatus = "nonlawofland";
        this.loader.dismiss();
      });
    });
  }

  downloadTP(att){
    this.js.getTP(att.id).then((obj) => {
      obj.subscribe((data: any) => {
        if(data.result == true){
          /*
          var element = document.createElement('a');
          element.setAttribute('href', data.payload.url);
          element.setAttribute('download', att.title + '.pdf');
          element.style.display = 'none';
          document.body.appendChild(element);
          element.click();
          document.body.removeChild(element);
          */
          var tI = this.inaB.create(data.payload.url, "_system", "location=no");
          tI.close();
        }else{
          this.presentToast(data.message);
        }
      })
    });
  }

  ngOnInit() {
  }

  reqPDF(){
    this.js.reqPDF(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.presentToast(data.message);
      });
    });
  }

  openCase(entry){
    this.navCtrl.goForward('/judgment/' + entry.id);
  }

  fullJudgment(){
    this.navCtrl.goForward('judgmentcontent/' + this.id + "?csphrases=" + btoa(this.phrases.join(',')));
  }

  bToC1(bool: Boolean){
    if(bool)
      return "primary";
    else
      return "dark";
  }

  getAlphabet(i){
    return (i >= 26 ? this.getAlphabet((i / 26 >> 0) - 1) : '') +  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[i % 26 >> 0];
  }

  copyContents(index: number){
    var processedText = "Headnote ";
    processedText += this.getAlphabet(index) + '. ';
    processedText += this.payload.headnotes[index];
    var ynn = this.payload.iconumber.split(' ');
    processedText += "\n\n" + this.payload.iconumber + " from Indian Cases Online (KLJ) " + "https://klj.im/j/" + ynn[0] + "/" + ynn[2];
    this.clipboard.copy(processedText);
  }


  async presentToast(message: string) {
    const toast = await this.tC.create({
      message: message,
      duration: 2500
    });
    toast.present();
  }
}
