import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JudgmentPage } from './judgment.page';

describe('JudgmentPage', () => {
  let component: JudgmentPage;
  let fixture: ComponentFixture<JudgmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JudgmentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JudgmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
