import { SearchService } from '../services/search.service';
import { NavController } from '@ionic/angular';
import {
  Component,
  OnInit
} from "@angular/core";
import { HttpErrorResponse } from '@angular/common/http';
import {
  LoadingController,
  ToastController
} from '@ionic/angular';

import $ from '../../../node_modules/jquery/dist/jquery';
import pubnub from '../../../node_modules/pubnub/dist/web/pubnub';
import selectize from '../../../node_modules/selectize/dist/js/selectize';

//declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: [
    './search.page.scss'
  ],
})
export class SearchPage implements OnInit {

  decade = "";
  results = [];
  query = {};
  textQs = [];
  jcount = 0;
  hcount = 0;
  curPage = 1;
  loading = false;
  loader = null;

  constructor(
    protected ss: SearchService,
    protected navCtrl: NavController,
    public tC: ToastController,
    public loadingController: LoadingController
  ) { 
  }

  ngOnInit() {
      console.log(selectize);
      $('.SLCTZ').each( function(i,l){
        $(this).selectize({
          plugins: ['remove_button'],
          maxItems: 50,
          valueField: 'iden',
          labelField: 'prompt',
          searchField: 'prompt',
          sortField: 'text',
          persist : true,
          create: false,
          loadOption: 'remote',
          load: function(query, callback){
            var self = this;
            if (!query.length) return callback();
            $.ajax({
              url: 'https://indiancases.com/api/searchList/' + $(l).attr('id'),
              type: 'GET',
              dataType: 'json',
              data: {
                q: query.trim(),
              },
              error: function() {
                callback();
              },
              success: function(res) {
                callback(res.data);
              }
            });
          },
          render: {
            option: function(item, escape) {
              return '<div>' + item.prompt + '</div>';
            }
          },
          onChange: function (){
            this.close();
          }
        }); 
      });
      this.search(1);
  }

  async search(page: number){
    this.loader = await this.loadingController.create({
      content: 'Loading Cases',
      duration: 200000
    });
    
    this.loader.present();
    this.loading = true;
    this.results = [];
    this.query = [ this.decade ].concat(
      $('#text').val().concat(
      $('#act').val().concat(
      $('#section').val().concat(
      $('#case').val().concat(
      $('#judge').val().concat(
      $('#party').val()
    ))))));
    await this.ss.getResults(this.query, page).then((obj) => {
      obj.subscribe((data: any) => {
      if(data.result == true){
        this.results = data.payload;
        this.jcount = data.meta.jcount;
        this.hcount = data.meta.hcount;
        this.curPage = page;
        this.loading = false;
        this.loader.dismiss();
      }else{
        this.presentToast(data.message);
        this.loader.dismiss();
        if(!data.login)
          window.location.href = "./auth/login";
      }
    },
    (err: HttpErrorResponse) => {
      this.loader.dismiss();
    });
  });
  }


  itemSelected(id: any){
    var tQs = [];
    for(var i=0; i< $('#text').val().length; i++)
      tQs.push($('#text')[0][i].text.trim());
    setTimeout(() => {
      this.navCtrl.goForward('/judgment/' + id + '?csphrases=' + btoa(tQs.join(',')));
    }, 1000);
  }

  getAlphabet(i){
    return (i >= 26 ? this.getAlphabet((i / 26 >> 0) - 1) : '') +  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[i % 26 >> 0];
  }

  async presentToast(message: string) {
    const toast = await this.tC.create({
      message: message,
      duration: 2500
    });
    toast.present();
  }
}
