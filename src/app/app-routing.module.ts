import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'yearList/:id', loadChildren: './citation/deep/year-list/year-list.module#YearListPageModule' },
  { path: 'volumeList/:id', loadChildren: './citation/deep/volume-list/volume-list.module#VolumeListPageModule' },
  { path: 'citationList/:id', loadChildren: './citation/deep/citation-list/citation-list.module#CitationListPageModule' },
  { path: 'judgment/:id', loadChildren: './judgment/judgment.module#JudgmentPageModule' },
  { path: 'judgmentcontent/:id', loadChildren: './judgmentcontent/judgmentcontent.module#JudgmentcontentPageModule' },
  { path: 'ico/:year/:number', loadChildren: './deeplink/dummy/ynn/ynn.module#YnnPageModule' },
  { path: 'auth/login', loadChildren: './auth/login/login.module#LoginPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
