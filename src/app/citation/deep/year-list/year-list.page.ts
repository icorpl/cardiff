import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CitationService } from '../../../services/citation.service';
import { NavController } from '../../../../../node_modules/@ionic/angular';

@Component({
  selector: 'app-year-list',
  templateUrl: './year-list.page.html',
  styleUrls: ['./year-list.page.scss'],
})
export class YearListPage implements OnInit {

  public id = "";
  public yearList = [];

  constructor(private route: ActivatedRoute, protected cs: CitationService, protected navCtrl: NavController) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadYears();
  }
  loadYears() {
    this.cs.getYears(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.yearList = data.payload;
        if(this.yearList.length == 1)
          this.navCtrl.goForward('/volumeList/' + this.yearList[0].id);
      })
    });
  }
  ngOnInit(){
  }
  itemSelected(entry: any){
    this.navCtrl.goForward('/volumeList/' + entry.id);
  }

}
