import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YearListPage } from './year-list.page';

describe('YearListPage', () => {
  let component: YearListPage;
  let fixture: ComponentFixture<YearListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
