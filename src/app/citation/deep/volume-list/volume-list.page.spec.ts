import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumeListPage } from './volume-list.page';

describe('VolumeListPage', () => {
  let component: VolumeListPage;
  let fixture: ComponentFixture<VolumeListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolumeListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolumeListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
