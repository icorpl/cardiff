import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { CitationService } from '../../../services/citation.service';
import { NavController } from '../../../../../node_modules/@ionic/angular';

@Component({
  selector: 'app-volume-list',
  templateUrl: './volume-list.page.html',
  styleUrls: ['./volume-list.page.scss'],
})
export class VolumeListPage implements OnInit {

  volumeList = [];
  id =null;

  constructor(private route: ActivatedRoute, protected cs: CitationService, protected navCtrl: NavController) { 
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadYears();
  }
  loadYears() {
    this.cs.getVolumes(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.volumeList = data.payload; 
        if(this.volumeList.length == 1)
          this.navCtrl.goForward('/citationList/' + this.volumeList[0].id);
      })
    });
  }
  ngOnInit(){
  }
  itemSelected(entry: any){
    this.navCtrl.goForward('/citationList/' + entry.id);
  }
}