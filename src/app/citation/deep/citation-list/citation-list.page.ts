import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { CitationService } from '../../../services/citation.service';
import { NavController } from '../../../../../node_modules/@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-citation-list',
  templateUrl: './citation-list.page.html',
  styleUrls: ['./citation-list.page.scss'],
})
export class CitationListPage implements OnInit {

  citationList = [];
  id =null;
  loading = null;

  constructor(private route: ActivatedRoute, protected cs: CitationService, protected navCtrl: NavController, public loadingController: LoadingController) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.loadYears();
  }
  loadYears() {
    //this.loader();
    this.cs.getCitations(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.citationList = data.payload;
      })
    });
  }
  async loader() {
    this.loading = await this.loadingController.create({
      content: 'Loading',
      duration: 20000
    });
  }
  ngOnInit() {
  }
  itemSelected(entry: any){
    this.navCtrl.goForward('/judgment/' + entry.id);
  }

}
