import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitationListPage } from './citation-list.page';

describe('CitationListPage', () => {
  let component: CitationListPage;
  let fixture: ComponentFixture<CitationListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitationListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
