import { Component, OnInit } from '@angular/core';
import { CitationService } from '../services/citation.service';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-citation',
  templateUrl: './citation.page.html',
  styleUrls: ['./citation.page.scss'],
})
export class CitationPage implements OnInit {

  journalList = [];
  loading = null;

  constructor(
    protected cs: CitationService,
    protected navCtrl: NavController,
    public loadingController: LoadingController
  ) {
    this.cs.getJournals().then((obj) => {
      obj.subscribe((data: any) => {
        this.journalList = data.payload;
      })
    });
  }

  async loader() {
    this.loading = await this.loadingController.create({
      content: 'Loading',
      duration: 200,
      dismissOnPageChange: true,
      spinner: 'crescent'
    });
  }

  ngOnInit() {
  }

  itemSelected(entry: any){
    this.navCtrl.goForward('/yearList/' + entry.id);
  }

}
