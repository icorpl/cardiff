import { Component } from '@angular/core';

import { 
  Platform,
  NavController,
  AlertController,
  ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { JudgmentPage } from './judgment/judgment.page';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  showedAlert: boolean;
  confirmAlert: any;
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private deeplinks: Deeplinks,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private tC: ToastController,
    private location: Location
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.showedAlert = false;
      this.platform.backButton.subscribe(() => {
          /*if (this.navCtrl.consumeTransition
              if (!this.showedAlert) {
                  this.confirmExitApp();
              } else {
                  this.showedAlert = false;
                  this.confirmAlert.dismiss();
              }
              */
          //this.nav.pop();
          
          switch(window.location.pathname){
            case '/':
            case '/tabs':
            case '/tabs/(store:store)':
            case '/tabs/(search:search)':
            case '/tabs/(citation:citation)':
            case '/tabs/(profile:profile)':
              this.navCtrl.consumeTransition;
              this.presentToast("Please use the home button to exit");
            break;
            default :
              this.location.back();
            break;
          }
      });

      this.deeplinks.routeWithNavController(this.navCtrl, {
        '/j/:year/:number': JudgmentPage,
        '/judgment/:id': JudgmentPage
      }).subscribe(match => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        if(typeof match != "undefined")
          if(typeof match.$args != "undefined"){
            if(typeof match.$args.id != "undefined")
              this.navCtrl.goForward('judgment/' + match.$args.id);
            if(typeof match.$args.year != "undefined")
              if(typeof match.$args.number != "undefined")
                this.navCtrl.goForward('ico/' + match.$args.year + '/' + match.$args.number);
          }
        /*alert(JSON.stringify(match));
        console.log('Successfully matched route', match);*/
      }, nomatch => {
        // nomatch.$link - the full link data
        /*alert(JSON.stringify(nomatch));
        console.error('Got a deeplink that didn\'t match', nomatch);*/
      });
    });
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.confirmAlert = this.alertCtrl.create({
        header: "Exit ?",
        subHeader: "Are you sure ?",
        message: "Click yes to confirm and exit the app!",
        buttons: [
            {
                text: 'No',
                handler: () => {
                    this.showedAlert = false;
                    return;
                }
            },
            {
                text: 'Yes',
                handler: () => {
                  //this.platform.exitApp();
                }
            }
        ]
    });
    this.confirmAlert.present();
  }

  async presentToast(message: string) {
    const toast = await this.tC.create({
      message: message,
      duration: 2500
    });
    toast.present();
  }
}