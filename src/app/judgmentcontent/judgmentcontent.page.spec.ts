import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JudgmentcontentPage } from './judgmentcontent.page';

describe('JudgmentcontentPage', () => {
  let component: JudgmentcontentPage;
  let fixture: ComponentFixture<JudgmentcontentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JudgmentcontentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JudgmentcontentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
