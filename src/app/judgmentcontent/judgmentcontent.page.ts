import {
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  NavController,
  LoadingController,
} from '@ionic/angular';
import { JudgmentService } from '../services/judgment.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-judgmentcontent',
  templateUrl: './judgmentcontent.page.html',
  styleUrls: ['./judgmentcontent.page.scss'],
})
export class JudgmentcontentPage implements OnInit {

  payload = {
    contents : "",
    hContents : ""
  };
  status = 'Judgment Loading';
  id = null;
  loader = null;
  phrases = [];
  extras = "";

  constructor(
    private route: ActivatedRoute,
    protected js: JudgmentService,
    protected navCtrl: NavController,
    public loadingController: LoadingController,
    private domSanitizer: DomSanitizer) {
      this.id = this.route.snapshot.paramMap.get('id');
      var thisER = this;
      this.route.queryParams
        .subscribe(params => {
          if(params['csphrases'] != null)
            thisER.phrases = atob(params['csphrases']).split(',');
          else
            thisER.phrases = [];
        });
        console.log(this.phrases)
      this.loadInfo();
  }

  bypSan(input: string){
    return this.domSanitizer.bypassSecurityTrustHtml(input);
  }

  async loadInfo(){
    this.loader = this.loader = await this.loadingController.create({
      content: 'Loading full text judgment...',
      duration: 200000
    });
    this.loader.present();

    this.js.getCaseContent(this.id).then((obj) => {
      obj.subscribe((data: any) => {
        this.payload.contents = data.payload.contents;
        this.payload.hContents = this.highlight();
        this.loader.dismiss();
      })
    });
  }

  searchChanged()
  {
    this.payload.hContents = this.highlight();
  }

  public highlight() {
    var temp = this.payload.contents;
    var thisER = this;
    this.phrases.forEach(function(item, index){
      if(item.trim().length)
        temp = temp.replace(new RegExp('(?<!</?[^>]*|&[^;]*)(' + item + ')', "gi"), match => {
          return '<span style="' + thisER.color(index) + '">' + match + '</span>';
        });
    });
    return temp;
  }

  ngOnInit(){
  }

  color(index: number){
    var colors = [
      "background: orange !important;",
      "background: #ff5555 !important;",
      "background: #7ec0ee !important;",
      "background: green !important;",
      "background: yellow !important;",
      "background: cyan !important;",
      "background: grey !important;"
    ];
    return colors[index%colors.length];
  }
}
