import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { JudgmentcontentPage } from './judgmentcontent.page';

const routes: Routes = [
  {
    path: '',
    component: JudgmentcontentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [JudgmentcontentPage]
})
export class JudgmentcontentPageModule {}
