import { TestBed, inject } from '@angular/core/testing';

import { JudgmentService } from './judgment.service';

describe('JudgmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JudgmentService]
    });
  });

  it('should be created', inject([JudgmentService], (service: JudgmentService) => {
    expect(service).toBeTruthy();
  }));
});
