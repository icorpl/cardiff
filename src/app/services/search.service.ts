import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})

export class SearchService {
  private searchUrl = 'api/v3/search/parametric2';

  constructor(private queryMaker: RequestService) {

  }
  public getResults(query: any, page: number) {
    return this.queryMaker.makeQuery(this.searchUrl, {
      'query' : query,
      'page' : page,
      'length' : 10
    });
  }
}