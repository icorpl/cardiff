import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class JudgmentService {
  private caseInfoUrl = 'api/v3/judgment/caseInfo';
  private caseContentUrl = 'api/v3/judgment/caseContent';
  private truePrintUrl = 'api/v3/judgment/truePrint';
  private reqPDFUrl = 'api/v3/judgment/pdf';
  
  constructor(private queryMaker: RequestService) {

  }
  public getCase(id: any) {
    return this.queryMaker.makeQuery(this.caseInfoUrl, {
      'id' : id
    });
  }
  public getCaseContent(id: any) {
    return this.queryMaker.makeQuery(this.caseContentUrl, {
      'id' : id
    });
  }
  public getTP(id: any) {
    return this.queryMaker.makeQuery(this.truePrintUrl, {
      'id' : id
    });
  }
  public reqPDF(id: any) {
    return this.queryMaker.makeQuery(this.reqPDFUrl, {
      'id' : id
    });
  }
  
}
