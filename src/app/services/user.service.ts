import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private detailsUrl = 'api/v3/user/getDetails';
  private sessionsUrl = 'api/v3/user/getSessions';

  constructor(private queryMaker: RequestService) {

  }

  public getDetails() {
    return this.queryMaker.makeQuery(this.detailsUrl, {
    });
  }

  public getSessions() {
    return this.queryMaker.makeQuery(this.sessionsUrl, {
    });
  }

}
