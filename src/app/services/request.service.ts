import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private baseUrl = [
    'https://indiancases.com/',
    'https://indiancases.tk/',
    'https://testing.indiancases.com/'
  ];
  token = "";
  
  constructor(
    private http: HttpClient,
    private storage: Storage,
    private appVersion: AppVersion
  ) {
    
  }

  public async makeQuery(url: any, body: any, server: number = 0){
    body.token = await this.storage.get('ic_token').then((val) => { return val; });
    body.app = {};
    body.app.appName = this.appVersion.getAppName
    body.app.pacakgeName = this.appVersion.getPackageName
    body.app.versionCode = this.appVersion.getVersionCode
    body.app.versionNumber = this.appVersion.getVersionNumber
    return this.http.post<any>(this.baseUrl[server] + url, body, {});
  }
}
