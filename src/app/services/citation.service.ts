import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})

export class CitationService {
  private journalListUrl = 'api/v3/citation/journalList';
  private yearListUrl = 'api/v3/citation/yearList';
  private volumeListUrl = 'api/v3/citation/volumeList';
  private entriesListUrl = 'api/v3/citation/citationlist';
  private resolveICOUrl = 'api/v3/citation/resolveICO'

  constructor(private queryMaker: RequestService) {

  }
  public getJournals() {
    return this.queryMaker.makeQuery(this.journalListUrl, {
    });
  }
  public getYears(id: any) {
    return this.queryMaker.makeQuery(this.yearListUrl, {
      'id' : id
    });
  }
  public getVolumes(id: any) {
    return this.queryMaker.makeQuery(this.volumeListUrl, {
      'id' : id
    });
  }
  public getCitations(id: any) {
    return this.queryMaker.makeQuery(this.entriesListUrl, {
      'id' : id
    });
  }
  public getEIDFromICO(year: string, number: string) {
    return this.queryMaker.makeQuery(this.resolveICOUrl, {
      'year' : year,
      'number' : number
    });
  }
}
