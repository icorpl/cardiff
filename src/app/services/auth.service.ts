import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { platform } from '../../environments/pconfig/inuse';
import { Device } from '@ionic-native/device/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private validationUrl = 'api/v3/auth/validate';
  private attachUrl = 'api/v3/auth/attach';
  private checkUrl = 'api/v3/auth/check';

  constructor(
    private rS: RequestService,
    private device: Device
  ) { }

  public validateSession() {
    return this.rS.makeQuery(this.validationUrl, {});
  }
  public attachDevice(id: string, password: string) {
    return this.rS.makeQuery(this.attachUrl, {
      input: id,
      password: password,
      device_class: platform.apiNotation,
      meta: {
        cordova: this.device.cordova,
        isVirtual: this.device.isVirtual,
        manufacturer: this.device.manufacturer,
        model: this.device.model,
        platform: this.device.platform,
        serial: this.device.serial,
        uuid: this.device.uuid,
        version: this.device.version,
      }
    });
  }
  public checkUser(id: string) {
    return this.rS.makeQuery(this.checkUrl, {
      input: id
    });
  }
}
