import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YnnPage } from './ynn.page';

describe('YnnPage', () => {
  let component: YnnPage;
  let fixture: ComponentFixture<YnnPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YnnPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YnnPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
