import { Component, OnInit } from '@angular/core';
import { CitationService } from '../../../services/citation.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-ynn',
  templateUrl: './ynn.page.html',
  styleUrls: ['./ynn.page.scss'],
})
export class YnnPage implements OnInit {

  year: string;
  number: string;

  constructor(
    private cs: CitationService,
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) {
    this.year = this.route.snapshot.paramMap.get('year');
    this.number = this.route.snapshot.paramMap.get('number');
  }

  ngOnInit() {
    this.cs.getEIDFromICO(this.year, this.number).then((obj) => {
      obj.subscribe((data: any) => {
        if(data.result)
          this.navCtrl.goForward('/judgment/' + data.payload.id);
        else
          this.navCtrl.goForward('/');
      });
    });
  }

}
