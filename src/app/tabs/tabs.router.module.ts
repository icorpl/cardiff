import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';
import { ProfilePage } from '../profile/profile.page';
import { CitationPage } from '../citation/citation.page';
import { SearchPage } from '../search/search.page';
import { StorePage } from '../store/store.page';
import { DiaryPage } from '../diary/diary.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'diary',
        outlet: 'diary',
        component: DiaryPage
      },
      {
        path: 'store',
        outlet: 'store',
        component: StorePage
      },
      {
        path: 'search',
        outlet: 'search',
        component: SearchPage
      },
      {
        path: 'citation',
        outlet: 'citation',
        component: CitationPage
      },
      {
        path: 'profile',
        outlet: 'profile',
        component: ProfilePage
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/(search:search)',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
