import {
    Component,
    OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import {
    NavController,
    LoadingController,
} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

    id = "";
    password: "";
    validUser = false;

    constructor(
        private aS: AuthService,
        private tC: ToastController,
        private sS: Storage,
        protected navCtrl: NavController
    ) { 
    }

    ngOnInit() {

    }

    validate(){
        this.aS.checkUser(this.id).then((obj) => {
            obj.subscribe((data: any) => {
                this.validUser = data.result;
                if(data.result){
                    this.presentToast(data.message + ". Please Login!")
                }else{
                    this.presentToast(data.message + ". Please Register!")
                }
            })
        });
    }

    async presentToast(message: string) {
        const toast = await this.tC.create({
          message: message,
          duration: 2500
        });
        toast.present();
    }

    login(){
        this.sS.set('ic_token', "");
        this.aS.attachDevice(this.id, this.password).then((obj) => {
            obj.subscribe((data: any) => {
                if(data.result){
                    this.presentToast("Login Successful, redirecting...");
                    this.sS.set('ic_token', data.payload.token);
                    this.sS.set('ic_sub', data.payload.sub);

                    this.sS.get('ic_token').then((data2: any) => {
                        if(data2 == data.payload.token){
                            this.sS.get('ic_sub').then((data3: any) => {
                                if(data3 == data.payload.sub){
                                    this.presentToast("Login Validated, redirecting...");
                                    window.location.href = "./"
                                }
                            })
                        }
                    })
                }
                else
                    this.presentToast(data.message);
            });
        });
    }
    register(){

    }
}   
