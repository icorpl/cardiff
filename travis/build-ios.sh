#!/bin/bash -v

set -e

# Build Ionic App for iOS
cordova platform add ios --nofetch

if [[ "$TRAVIS_BRANCH" == "develop" ]]
then
    ionic cordova build ios
else
    cp src/environments/pconfig/ios.ts src/environments/pconfig/inuse.ts
    sudo ionic cordova build ios \
        --aot --minifyjs --minifycss --optimizejs \
        --prod --release --device \
        --buildConfig="build/config.json"
fi