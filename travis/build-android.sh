#!/bin/bash -v

set -e

# Build Ionic App for Android
# cordova platform add android --nofetch

if [[ "$TRAVIS_BRANCH" == "develop" ]]
then
    ionic cordova build android
else
    cp src/environments/pconfig/android.ts src/environments/pconfig/inuse.ts
    sudo ionic cordova build android --verbose \
        --aot --minifyjs --minifycss --optimizejs --prod --release --device \
        -- -- \
        --cdvBuildMultipleApks \
        --cdvReleaseSigningPropertiesFile \
        --keystore=config/csf/android/keystore.jks \
        --alias="$config_android_alias" \
        --storePassword="$config_android_storePassword" \
        --password="$config_android_password"
fi